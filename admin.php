<?php
    include('session.php');
    include('connection.php');

$activities = mysqli_query($conn,"SELECT * FROM `category`;");
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Asociația Sportivă Școlară</title>

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="custom.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" style="background-color: rgb(8,33,95)!important;">
    <div class="container">
        <a class="navbar-brand" href="<?php echo $base_url; ?>">Asociația Sportivă Școlară</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $base_url; ?>">Acasă
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <!-- Dropdown -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        Activități
                    </a>
                    <div class="dropdown-menu">
                        <?php
                        foreach($activities as $row) {
                            ?>
                            <a class="dropdown-item" href="<?php echo $base_url; ?>activityview.php?id=<?php echo $row['id'];?>"><?php echo $row['title']; ?></a>
                            <?php
                        }
                        ?>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $base_url; ?>logout.php">Logout
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo $base_url; ?>admin.php">Admin
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Modal -->
<div class="modal fade" id="addCategModal" tabindex="-1" role="dialog" aria-labelledby="addCategModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Adauga Categorie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Titlu Categorie</label>
                    <input type="text" id="categaddtitle" class="form-control" placeholder="Titlu...">
                </div>
                <div class="form-group">
                    <button class="btn btn-success btn-block" onclick="window.location.href = '<?php echo $base_url; ?>addcateg.php?title='+$('#categaddtitle').val();">Adauga</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Header - set the background image for the header in the line below -->
<header class="py-5 bg-image-full" style="background-color: rgb(8,33,95);">
    <img class="img-fluid d-block mx-auto" src="logoliisyes.png"  alt="">
</header>

<!-- Content section -->
<section class="py-5">
    <div class="container">
        <form action="<?php echo $base_url; ?>addsection.php" method="post" enctype="multipart/form-data">
            <!--Add a section-->
            <div class="form-group">
                <label for="">Adauga o sectiune</label>
                <div class="input-group">
                    <select name="category_id" class="form-control" id="selectCategory" required>
                        <option selected disabled value="dont">Selecteaza o categorie...</option>
                        <?php
                        foreach($activities as $row) {
                            ?>
                            <option value="<?php echo $row['id']; ?>"><?php echo $row['title']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" type="button" data-toggle="modal" data-target="#addCategModal" id="categAddButton"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <input type="text" name="title" class="form-control" placeholder="Titlu...">
            </div>
            <div class="form-group">
                <input type="text" name="subtitle" class="form-control" placeholder="Subtitlu...">
            </div>
            <div class="form-group">
                <textarea name="content" id="" cols="30" rows="10" class="form-control" placeholder="Continut..." style="max-width: 100%;min-width: 100%; max-height: 300px;"></textarea>
            </div>
            <div class="form-group">
                <div class="form-group">
                    <label for="exampleFormControlFile1">Imagine(i)</label>
                    <input type="file" name="images" class="form-control-file" id="exampleFormControlFile1" required>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-block btn-success">Adauga</button>
            </div>
        </form>
        <br><br><br>
        <div class="form-group">
            <label for="">Sterge o categorie:</label>
            <div class="input-group">
                <select class="custom-select" id="categoryRemoveSelect" onchange="$('#categoryRemoveButton').attr('disabled',false);">
                    <option selected disabled value="dont">Selecteaza o categorie care vrei sa o stergi...</option>
                    <?php
                    foreach($activities as $row) {
                        ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['title']; ?></option>
                        <?php
                    }
                    ?>
                </select>
                <div class="input-group-append">
                    <button class="btn btn-outline-danger" type="button" id="categoryRemoveButton" onclick="window.location.href = '/stan/removecateg.php?id='+$('#categoryRemoveSelect').val();" disabled><i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="">Sterge o sectiune:</label>
            <div class="input-group">
                <select class="form-control" id="inputGroupSelect" onchange="updateSectionSelectOptions($(this).val())">
                    <option selected disabled>Selecteaza Categorie...</option>
                    <?php
                    foreach($activities as $row) {
                        ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['title']; ?></option>
                        <?php
                    }
                    ?>
                </select>
                <select class="form-control" id="sectionRemoverSelect" onchange="$('#removeSectionButton').attr('disabled',false);">
                    <option selected disabled>Selecteaza Sectiune...</option>

                </select>
                <div class="input-group-append">
                    <button class="btn btn-outline-danger" type="button" disabled id="removeSectionButton" onclick="window.location.href = '/stan/removesection.php?id='+$('#sectionRemoverSelect').val();"><i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>

        <script>
            function updateSectionSelectOptions(id){
                $.get('/stan/getSections.php?id='+id, function(data){
                   let info = JSON.parse(data);
                   console.log(info);
                    $('#sectionRemoverSelect').empty();
                   $('#sectionRemoverSelect').append("<option selected disabled>Selecteaza Sectiune...</option>");
                   for(let i = 0; i < info.length; i++){
                       $('#sectionRemoverSelect').append('<option value="'+info[i]['id']+'">'+info[i]['headeer']+"</option>");
                   }
                });
            }
        </script>
    </div>
</section>


<!-- Footer -->
<footer class="py-5 bg-dark" style="background-color: rgb(8,33,95)!important;">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; maze-team.com 2018</p>
    </div>
    <!-- /.container -->
</footer>


</body>
</html>
