<?php


require_once "connection.php";
require_once "getActivities.php";
session_start();

if(isset($_SESSION['login_user'])){
    header("Location: admin.php");
}


//$sql = "SELECT * FROM users WHERE username = 'admin' and password = 'Liis1234'";
//$result = mysqli_query($conn,$sql);
//foreach(mysqli_fetch_array($result,MYSQLI_ASSOC) as $row){
//    echo $row . " ";
//}


if($_SERVER["REQUEST_METHOD"] == "POST") {
    // username and password sent from form

    $myusername = mysqli_real_escape_string($conn,$_POST['username']);
    $mypassword = mysqli_real_escape_string($conn,$_POST['pass']);

    $sql = "SELECT * FROM users WHERE username = '$myusername' and password = '$mypassword'";
    $result = mysqli_query($conn,$sql);
    $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
    $active = $row['active'];

    $count = mysqli_num_rows($result);
    echo $count;
    // If result matched $myusername and $mypassword, table row must be 1 row

    if($count == 1) {
        //session_register("myusername");
        $_SESSION['login_user'] = $myusername;

        header("location: admin.php");
    }else {
        echo "Your Login Name or Password is invalid";
    }
}
?>

    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Asociația Sportivă Școlară</title>

        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <!-- Custom styles for this template -->
        <link href="custom.css" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" style="background-color: rgb(8,33,95)!important;">
        <div class="container">
            <a class="navbar-brand" href="<?php echo $base_url; ?>">Asociația Sportivă Școlară</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $base_url; ?>">Acasă
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <!-- Dropdown -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                            Activități
                        </a>
                        <div class="dropdown-menu">
                            <?php
                            foreach($activities as $row) {
                                ?>
                                <a class="dropdown-item" href="<?php echo $base_url; ?>activityview.php?id=<?php echo $row['id'];?>"><?php echo $row['title']; ?></a>
                                <?php
                            }
                            ?>
                        </div>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo $base_url; ?>login.php">Login
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Header - set the background image for the header in the line below -->
    <header class="py-5 bg-image-full" style="background-color: rgb(8,33,95);">
        <img class="img-fluid d-block mx-auto" src="logoliisyes.png"  alt="">
    </header>

    <!-- Content section -->
    <section class="py-5">
        <div class="container">
            <h1>Login</h1>
            <form action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
                <div class="form-group">
                    <input type="text" name="username" maxlength="40" class="form-control" placeholder="Username...">
                </div>
                <div class="form-group">
                    <input type="password" name="pass" maxlength="50" class="form-control" placeholder="Password...">
                </div>
                <div class="form-group">
                    <input type="submit" name="submit" value="Login" class="btn btn-primary btn-block">
                </div>
            </form>
        </div>
    </section>

    <!-- Footer -->
    <footer class="py-5 bg-dark" style="background-color: rgb(8,33,95)!important;">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; maze-team.com 2018</p>
        </div>
        <!-- /.container -->
    </footer>

    </body>
    </html>

