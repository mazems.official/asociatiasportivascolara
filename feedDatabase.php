<?php
require_once "connection.php";

// sql to create table
$create_media_table = "CREATE TABLE IF NOT EXISTS media (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
url MEDIUMTEXT NOT NULL
)";

$create_user_table = "CREATE TABLE IF NOT EXISTS users (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
username VARCHAR(30) NOT NULL,
password VARCHAR(150) NOT NULL
)";

$create_section_table = "CREATE TABLE IF NOT EXISTS sections (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
headeer VARCHAR(200) NULL,
subheader VARCHAR(300) NULL,
content LONGTEXT NULL,
image_ids MEDIUMTEXT NOT NULL,
category_id INT(6) NOT NULL
)";

$create_category_table = "CREATE TABLE IF NOT EXISTS category (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
title VARCHAR(200) NOT NULL
)";

if (mysqli_query($conn, $create_media_table) && mysqli_query($conn, $create_user_table) && mysqli_query($conn, $create_section_table) && mysqli_query($conn, $create_category_table) ) {
} else {
    echo "Error creating table: " . mysqli_error($conn);
}

$ok = mysqli_query($conn, "INSERT INTO `users` (`username`, `password`) VALUES ('admin', 'Liis1234')");

$sql = "INSERT INTO category (`title`) VALUES ('Sport in pauza mare 2017')";
mysqli_query($conn, $sql);

?>