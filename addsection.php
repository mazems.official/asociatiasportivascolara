<?php

include('connection.php');
$id = 0;

// Check if file was uploaded without errors
if(isset($_FILES["images"]) && $_FILES["images"]["error"] == 0){
    $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
    $filename = $_FILES["images"]["name"];
    $filetype = $_FILES["images"]["type"];
    $filesize = $_FILES["images"]["size"];

    // Verify file extension
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    $filename = generateRandomString(10) . "." . $ext;
    echo $filename . "\n";
    if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format. (jpg/jpeg/gif/png)");

    // Verify file size - 5MB maximum
    $maxsize = 5 * 1024 * 1024;
    if($filesize > $maxsize) die("Error: File size is larger than the allowed limit. (5MB)");

    // Verify MYME type of the file
    if(in_array($filetype, $allowed)){
        // Check whether file exists before uploading it
        if(file_exists("images/" . $filename)){
            echo $filename . " is already exists.";
        } else{
            move_uploaded_file($_FILES["images"]["tmp_name"], "images/" . $filename);
            if(mysqli_query($conn, "INSERT INTO media (`url`) VALUES ('images/".$filename."')")){
                $id = mysqli_fetch_assoc(mysqli_query($conn, "SELECT id FROM media WHERE url = 'images/".$filename."';"));
                $sql = "INSERT INTO `sections`(`headeer`, `subheader`, `content`, `image_ids`, `category_id`) VALUES ('".$_POST['title']."','".$_POST['subtitle']."','".$_POST['content']."','".$id["id"]."','".$_POST['category_id']."')";
                echo $sql . "\n";

//var_dump($sql);
                if(mysqli_query($conn, $sql)){
                    header("Location: admin.php");
                }else{
                    echo "Failed";
                    return;
                }
            }
            echo "Your file was uploaded successfully.";
        }
    } else{
        echo "Error: There was a problem uploading your file. Please try again.";
    }
} else{
    echo "Error: " . $_FILES["images"]["error"];
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

?>